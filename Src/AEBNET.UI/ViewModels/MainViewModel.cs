// ==============================================================================
//  AEBNET
//
//  Jos� �ngel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using AEBNET.Core;
using AEBNET.UI.Views;
using FirstFloor.ModernUI.Windows.Controls;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace AEBNET.UI.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private Liquidaciones liquidaciones = null;
        private CollectionView collectionView = null;
        private LoadingView loadingView = null;

        public Action<int> CustomizeColumns { get; set; }

        #region Private
        async Task StartLoadAsync(
            Modalidades modalidad,
            string fileName,
            CancellationToken token,
            IProgress<int> progress)
        {
            try
            {
                var liquidacionesTmp = await C60.LoadLiquidacionesAsync(modalidad, fileName, token, progress);

                if (!token.IsCancellationRequested)
                {
                    liquidaciones = liquidacionesTmp;

                    if (loadingView != null)
                    {
                        loadingView.Close();
                    }

                    UpdateViewAfterLoad(modalidad);
                }
            }
            catch (Exception exception)
            {
                ModernDialog.ShowMessage(exception.Message, "ERROR", MessageBoxButton.OK);
            }
            finally
            {
                if (loadingView != null)
                {
                    loadingView.Close();
                }
            }
        }

        private void UpdateViewAfterLoad(Modalidades modalidad)
        {
            string sort = "Referencia";

            switch (modalidad)
            {
                case Modalidades.M1:
                    ItemsSourceProperty = liquidaciones.ToCollection<RegistroModalidad1>();
                    break;

                case Modalidades.M2:
                    ItemsSourceProperty = liquidaciones.ToCollection<RegistroModalidad2>();
                    break;

                case Modalidades.M3:
                    ItemsSourceProperty = liquidaciones.ToCollection<RegistroModalidad3>();
                    sort = "NumeroExpediente";
                    break;
            }

            // Get default view
            collectionView = (CollectionView)CollectionViewSource.GetDefaultView(ItemsSourceProperty);
            collectionView.SortDescriptions.Add(new SortDescription(sort, ListSortDirection.Ascending));

            CustomizeColumns((int)modalidad);

            collectionView.MoveCurrentToFirst();
            SelectedItemProperty = collectionView.CurrentItem;

            if (liquidaciones.Registros.Count > 0)
            {
                DetailsVisibilityProperty = Visibility.Visible;
            }
            else
            {
                DetailsVisibilityProperty = Visibility.Hidden;
            }

            SaveCommand.RaiseCanExecuteChanged();
        }
        #endregion

        #region Property: SelectedItem
        public const string SelectedItemPropertyName = "SelectedItemProperty";
        private object selectedItem = null;

        public object SelectedItemProperty
        {
            get
            {
                return selectedItem;
            }
            set
            {
                if (selectedItem == value)
                {
                    return;
                }

                selectedItem = value;
                RaisePropertyChanged(SelectedItemPropertyName);
            }
        }
        #endregion

        #region Property: ItemsSource
        public const string ItemsSourcePropertyName = "ItemsSourceProperty";
        private IList itemsSource;

        public IList ItemsSourceProperty
        {
            get
            {
                return itemsSource;
            }
            set
            {
                if (itemsSource == value)
                {
                    return;
                }

                itemsSource = value;
                RaisePropertyChanged(ItemsSourcePropertyName);
            }
        }
        #endregion

        #region Property: Details
        public const string DetailsPropertyName = "DetailsProperty";
        private string details = "";

        public string DetailsProperty
        {
            get
            {
                return details;
            }
            set
            {
                if (details == value)
                {
                    return;
                }

                details = value;
                RaisePropertyChanged(DetailsPropertyName);
            }
        }
        #endregion

        #region Property: StatusBarText
        public const string StatusBarTextPropertyName = "StatusBarTextProperty";
        private string statusBarText = "No data";

        public string StatusBarTextProperty
        {
            get
            {
                return statusBarText;
            }
            set
            {
                if (statusBarText == value)
                {
                    return;
                }

                statusBarText = value;
                RaisePropertyChanged(StatusBarTextPropertyName);
            }
        }
        #endregion

        #region Property: DetailsVisibility
        public const string DetailsVisibilityPropertyName = "DetailsVisibilityProperty";
        private Visibility detailsVisibility = Visibility.Hidden;

        public Visibility DetailsVisibilityProperty
        {
            get
            {
                return detailsVisibility;
            }
            set
            {
                if (detailsVisibility == value)
                {
                    return;
                }

                detailsVisibility = value;
                RaisePropertyChanged(DetailsVisibilityPropertyName);
            }
        }
        #endregion

        #region Command: Import
        private RelayCommand importCommand;

        public RelayCommand ImportCommand
        {
            get
            {
                return importCommand ?? (importCommand = new RelayCommand(
                    ExecuteImportCommand,
                    CanExecuteImportCommand));
            }
        }

        public void ExecuteImportCommand()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Title = "Import File C60-ModX";
            dlg.RestoreDirectory = true;
            dlg.DefaultExt = ".val";
            dlg.Filter =
                "C60-Mod1 Files (*.val)|*.val|" +
                "C60-Mod2 Files (*.val)|*.val|" +
                "C60-Mod3 Files (*.val)|*.val";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                try
                {
                    loadingView = new LoadingView();

                    // Update the ViewModel
                    var loadingViewModel = new ViewModelLocator().Loading;
                    loadingViewModel.Title = "LOADING FILE: " + Path.GetFileName(dlg.FileName);
                    loadingViewModel.Details = "Processing line...";

                    // Configure the cancellation token
                    var cts = new CancellationTokenSource();
                    loadingView.Closed += (s, e) => cts.Cancel();

                    // Configure the progress indicator
                    var progressIndicator = new Progress<int>((i) =>
                    {
                        loadingViewModel.Details = String.Format("Processing line {0:N0}...", i);
                    });

                    // Launch the task
                    var task = StartLoadAsync((Modalidades)dlg.FilterIndex, dlg.FileName, cts.Token, progressIndicator);

                    loadingView.ShowDialog();
                }
                catch (Exception exception)
                {
                    ModernDialog.ShowMessage(exception.Message, "ERROR", MessageBoxButton.OK);
                }
            }
        }

        private bool CanExecuteImportCommand()
        {
            return true;
        }
        #endregion

        #region Command: Save
        private RelayCommand saveCommand;

        public RelayCommand SaveCommand
        {
            get
            {
                return saveCommand ?? (saveCommand = new RelayCommand(
                    ExecuteSaveCommand,
                    CanExecuteSaveCommand));
            }
        }

        private void ExecuteSaveCommand()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Title = "Save To CSV";
            dlg.FileName = "Document";
            dlg.RestoreDirectory = true;
            dlg.DefaultExt = ".csv";
            dlg.Filter = "CSV Files (*.csv)|*.csv";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                liquidaciones.ExportToCSV(dlg.FileName);
            }
        }

        private bool CanExecuteSaveCommand()
        {
            if (liquidaciones != null)
            {
                return (liquidaciones.Registros.Count > 0);
            }

            return false;
        }
        #endregion

        #region Command: SelectionChanged
        private RelayCommand selectionChangedCommand;

        public RelayCommand SelectionChangedCommand
        {
            get
            {
                return selectionChangedCommand ?? (selectionChangedCommand = new RelayCommand(
                    ExecuteSelectionChangedCommand,
                    CanExecuteSelectionChangedCommand));
            }
        }

        private void ExecuteSelectionChangedCommand()
        {
            DetailsProperty = SelectedItemProperty.ToString();

            StatusBarTextProperty =
                "Record: " + (collectionView.IndexOf(SelectedItemProperty) + 1).ToString("N0") +
                " of " + collectionView.Count.ToString("N0");
        }

        private bool CanExecuteSelectionChangedCommand()
        {
            return (SelectedItemProperty != null);
        }
        #endregion
    }
}