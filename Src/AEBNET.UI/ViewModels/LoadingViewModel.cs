﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using GalaSoft.MvvmLight;

namespace AEBNET.UI.ViewModels
{
    public class LoadingViewModel : ViewModelBase
    {
        public LoadingViewModel()
        {

        }

        #region Property: Title
        public const string TitlePropertyName = "Title";
        private string title = "TITLE";

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                if (title == value)
                {
                    return;
                }

                title = value;
                RaisePropertyChanged(TitlePropertyName);
            }
        }
        #endregion

        #region Property: Details
        public const string DetailsPropertyName = "Details";
        private string details = "Details...";

        public string Details
        {
            get
            {
                return details;
            }
            set
            {
                if (details == value)
                {
                    return;
                }

                details = value;
                RaisePropertyChanged(DetailsPropertyName);
            }
        }
        #endregion
    }
}