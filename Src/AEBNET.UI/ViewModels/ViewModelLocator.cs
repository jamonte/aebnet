// ==============================================================================
//  AEBNET
//
//  Jos� �ngel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace AEBNET.UI.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<LoadingViewModel>();
        }

        #region ViewModel: Main
        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }
        #endregion

        #region ViewModel: Loading
        public LoadingViewModel Loading
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LoadingViewModel>();
            }
        }
        #endregion

        public static void Cleanup()
        {

        }
    }
}
