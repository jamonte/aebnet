﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using FirstFloor.ModernUI.Windows.Controls;
using System.Windows.Controls;

namespace AEBNET.UI.Views
{
    /// <summary>
    /// Interaction logic for LoadingView.xaml
    /// </summary>
    public partial class LoadingView : ModernDialog
    {
        public LoadingView()
        {
            InitializeComponent();

            // define the dialog buttons
            this.Buttons = new Button[] { this.CancelButton };
        }
    }
}
