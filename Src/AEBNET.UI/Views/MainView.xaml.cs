﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using AEBNET.UI.ViewModels;
using FirstFloor.ModernUI.Windows.Controls;
using System;

namespace AEBNET.UI.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : ModernWindow
    {
        #region Add Columns
        private void AddColumnsM1()
        {
            HelperView.AddColumnToDataGrid(dataGridMain, "Emisor", "OrganismoEmisor", 80, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Referencia", "Referencia", 110, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Entidad", "EntidadRecaudadora", 90, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Oficina", "OficinaRecaudadora", 80, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Fecha Cobro", "FechaCobro", 110, DataGridAling.Center, "dd/MM/yyyy");
            HelperView.AddColumnToDataGrid(dataGridMain, "Importe", "Importe", 90, DataGridAling.Right, "C");
            HelperView.AddColumnToDataGrid(dataGridMain, "Modalidad Pago", "ModalidadPago", 140, DataGridAling.Center);
            //HelperView.AddColumnToDataGrid(dataGridMain, "Domiciliacion", "Domiciliacion", 110, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "CCC Domiciliacion", "CCCDomiciliacion", 180, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Tributo", "CodigoTributo", 80, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Ejercicio", "Ejercicio", 90, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Remesa", "Remesa", 90, DataGridAling.Center);
        }

        private void AddColumnsM2()
        {
            HelperView.AddColumnToDataGrid(dataGridMain, "Emisor", "OrganismoEmisor", 80, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Referencia", "Referencia", 110, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Entidad", "EntidadRecaudadora", 90, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Oficina", "OficinaRecaudadora", 80, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Fecha Cobro", "FechaCobro", 110, DataGridAling.Center, "dd/MM/yyyy");
            HelperView.AddColumnToDataGrid(dataGridMain, "Importe", "Importe", 90, DataGridAling.Right, "C");
            HelperView.AddColumnToDataGrid(dataGridMain, "Modalidad Pago", "ModalidadPago", 140, DataGridAling.Center);
            //HelperView.AddColumnToDataGrid(dataGridMain, "Domiciliacion", "Domiciliacion", 90, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "CCC Domiciliacion", "CCCDomiciliacion", 180, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Tributo", "CodigoTributo", 80, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Ejercicio", "Ejercicio", 90, DataGridAling.Center);
            //HelperView.AddColumnToDataGrid(dataGridMain, "Ano", "AnoVencimiento", 60, DataGridAling.Center);
            //HelperView.AddColumnToDataGrid(dataGridMain, "Fecha Juliana", "FechaJuliana", 90, DataGridAling.Center);
            //HelperView.AddColumnToDataGrid(dataGridMain, "Periodo", "DiscriminantePeriodo", 60, DataGridAling.Center);
        }

        private void AddColumnsM3()
        {
            HelperView.AddColumnToDataGrid(dataGridMain, "Emisor", "OrganismoEmisor", 80, DataGridAling.Center);
            //HelperView.AddColumnToDataGrid(dataGridMain, "Expediente", "NumeroExpediente", 110, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Entidad", "EntidadRecaudadora", 90, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Oficina", "OficinaRecaudadora", 80, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Fecha Cobro", "FechaCobro", 110, DataGridAling.Center, "dd/MM/yyyy");
            HelperView.AddColumnToDataGrid(dataGridMain, "Importe", "Importe", 90, DataGridAling.Right, "C");
            HelperView.AddColumnToDataGrid(dataGridMain, "Num. Justificante", "NumeroJustificante", 150, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "NIF/CIF", "NifCif", 90, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "Modelo", "CodigoModelo", 120, DataGridAling.Center);
            HelperView.AddColumnToDataGrid(dataGridMain, "F. Devengo", "FechaDevengo", 120, DataGridAling.Center, "dd/MM/yyyy");
            HelperView.AddColumnToDataGrid(dataGridMain, "Dato Especifico", "DatoEspecifico", 200);
        }
        #endregion

        public MainView()
        {
            InitializeComponent();

            Title = HelperView.FullTitle;

            // Actions
            MainViewModel viewModel = new ViewModelLocator().Main;
            viewModel.CustomizeColumns = new Action<int>((index) => this.CustomizeColumns(index));
        }

        private void CustomizeColumns(int index)
        {
            dataGridMain.Columns.Clear();

            switch (index)
            {
                case 1:
                    AddColumnsM1();
                    break;

                case 2:
                    AddColumnsM2();
                    break;

                case 3:
                    AddColumnsM3();
                    break;

                default:
                    break;
            }
        }
    }
}
