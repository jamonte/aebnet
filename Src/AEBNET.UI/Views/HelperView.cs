﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace AEBNET.UI.Views
{
    public static class HelperView
    {
        /// <summary>
        /// Gets the application title from the assembly.
        /// </summary>
        public static string Title
        {
            get
            {
                Assembly assembly = Assembly.GetEntryAssembly();
                AssemblyTitleAttribute attribute;

                attribute = (AssemblyTitleAttribute)AssemblyTitleAttribute.GetCustomAttribute(
                    assembly,
                    typeof(AssemblyTitleAttribute));

                return attribute.Title;
            }
        }

        /// <summary>
        /// Gets the application description from the assembly.
        /// </summary>
        public static string Descripcion
        {
            get
            {
                Assembly assembly = Assembly.GetEntryAssembly();
                AssemblyDescriptionAttribute attribute;

                attribute = (AssemblyDescriptionAttribute)AssemblyDescriptionAttribute.GetCustomAttribute(
                    assembly,
                    typeof(AssemblyDescriptionAttribute));

                return attribute.Description;
            }
        }

        /// <summary>
        /// Gets the application version from the assembly.
        /// </summary>
        public static string Version
        {
            get
            {
                Version version = Assembly.GetEntryAssembly().GetName().Version;

                return
                    version.Major.ToString() + "." +
                    version.Minor.ToString() + "." +
                    version.Build.ToString();
            }
        }

        /// <summary>
        /// Gets the application title, description and version  as string.
        /// </summary>
        public static string FullTitle
        {
            get
            {
                return Title + " | " + Descripcion + " " + Version;
            }
        }

        /// <summary>
        /// Add a column to a DataGrid.
        /// </summary>
        /// <param name="dataGrid">The DataGrid.</param>
        /// <param name="header">The column header.</param>
        /// <param name="binding">The binding.</param>
        /// <param name="width">The column width.</param>
        /// <param name="align">The column align.</param>
        /// <param name="format">The string format.</param>
        public static void AddColumnToDataGrid(
            DataGrid dataGrid,
            string header,
            string binding,
            int width,
            DataGridAling align = DataGridAling.Left,
            string format = "")
        {
            DataGridTextColumn textColumn = new DataGridTextColumn();

            textColumn.Header = header;
            textColumn.Binding = new Binding(binding);
            textColumn.Width = width;
            textColumn.ElementStyle = (Style)Application.Current.Resources["Align" + align.ToString()];

            if (format != "")
            {
                textColumn.Binding.StringFormat = format;
            }

            dataGrid.Columns.Add(textColumn);
        }
    }
}
