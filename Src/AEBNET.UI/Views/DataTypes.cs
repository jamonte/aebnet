﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

namespace AEBNET.UI.Views
{
    public enum DataGridAling
    {
        Left,
        Center,
        Right
    }
}
