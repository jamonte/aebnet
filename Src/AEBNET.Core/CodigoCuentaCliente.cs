﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System;

namespace AEBNET.Core
{
    /// <summary>
    /// Class CodigoCuentaCliente.
    /// </summary>
    public class CodigoCuentaCliente
    {
        public string Entidad { get; private set; }
        public string Oficina { get; private set; }
        public string DigitoControl { get; private set; }
        public string NumeroCuenta { get; private set; }

        /// <summary>
        /// Checks the digit control for the CodigoCuentaCliente.
        /// </summary>
        /// <param name="CCC">The string with a CodigoCuentaCliente.</param>
        /// <returns>The digit control for the CodigoCuentaCliente.</returns>
        private int CheckDigitoControl(
            string CCC)
        {
            int[] values = new int[] { 1, 2, 4, 8, 5, 10, 9, 7, 3, 6 };
            int control = 0;

            for (int i = 0; i < 10; i++)
            {
                control += Convert.ToInt32(CCC[i]) * values[i];
            }

            control = 11 - (control % 11);

            if (control == 11)
            {
                control = 0;
            }
            else if (control == 10)
            {
                control = 1;
            }

            return control;
        }

        /// <summary>
        /// CodigoCuentaCliente constructor.
        /// </summary>
        /// <param name="CCC">String with a CodigoCuentaCliente, 20 digits.</param>
        public CodigoCuentaCliente(
            string CCC)
        {
            // Remove all dashes, dots and white spaces
            string tmp = CCC.Replace("-", "").Replace(".", "").Replace(" ", "");

            if (tmp.Length != 20)
            {
                throw new Exception("The lenght of CCC must be 20 characters.");
            }

            Entidad = tmp.Substring(0, 4);
            Oficina = tmp.Substring(4, 4);
            DigitoControl = tmp.Substring(8, 2);
            NumeroCuenta = tmp.Substring(10, 10);

            int dc1 = Convert.ToInt32(DigitoControl[0].ToString());
            int dc2 = Convert.ToInt32(DigitoControl[1].ToString());

            // Check Digito Control
            if ((CheckDigitoControl("00" + Entidad + Oficina) != dc1) ||
                (CheckDigitoControl(NumeroCuenta) != dc2))
            {
                throw new Exception(String.Format("Error checking Digito Control: {0}" + this.ToString()));
            }
        }

        /// <summary>
        /// Default CodigoCuentaCliente constructor.
        /// </summary>
        public CodigoCuentaCliente()
            : this("00000000000000000000")
        {

        }
        
        /// <summary>
        /// Gets a string with de CodigoCuentaCliente using white space as separator.
        /// </summary>
        /// <returns>The string with the CodigoCuentaCliente.</returns>
        public override string ToString()
        {
            return Entidad + " " + Oficina + " " + DigitoControl + " " + NumeroCuenta;
        }
    }
}
