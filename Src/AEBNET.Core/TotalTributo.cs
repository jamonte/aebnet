﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

namespace AEBNET.Core
{
    /// <summary>
    /// Class TotalTributo.
    /// </summary>
    public class TotalTributo
    {
        public string OrganismoEmisor { get; set; }
        public int NumeroCobros { get; set; }
        public decimal TotalImporte { get; set; }
        public string CodigoTributo { get; set; }

        /// <summary>
        /// Gets the TotalTributo string.
        /// </summary>
        /// <returns>TotalTributo string.</returns>
        public override string ToString()
        {
            return
                " - Organismo Emisor: " + OrganismoEmisor + "\r\n" +
                " - Numero Cobros: " + NumeroCobros + "\r\n" +
                " - Total Importe: " + TotalImporte.ToString("C") + "\r\n" +
                " - Codigo Tributo: " + CodigoTributo +
                " (" + Helper.CodigoTributoToString(CodigoTributo) + ")\r\n";
        }
    }
}
