﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System.Collections.Generic;

namespace AEBNET.Core
{
    /// <summary>
    /// Class OrganismoEmisor.
    /// </summary>
    public class OrganismoEmisor
    {
        public CabeceraOrganismoEmisor Cabecera { get; set; }
        public List<Registros> Registros { get; set; }

        public OrganismoEmisor()
        {
            Registros = new List<Registros>();
        }
    }
}
