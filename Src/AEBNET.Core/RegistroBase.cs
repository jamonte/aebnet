﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

namespace AEBNET.Core
{
    /// <summary>
    /// Class RegistroBase.
    /// </summary>
    public abstract class RegistroBase
    {
        public abstract string ToHeaderCSV();
        public abstract string ToRecordCSV();
    }
}
