﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

namespace AEBNET.Core
{
    public enum Modalidades
    {
        M1 = 1,
        M2 = 2,
        M3 = 3
    }

    public enum ModalidadesPago
    {
        Ventanilla = 1,
        Autoservicio = 2,
        BancaVirtual = 3
    }
}
