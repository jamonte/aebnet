﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System;
using System.Collections.Generic;

namespace AEBNET.Core
{
    /// <summary>
    /// Class Helper.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Converts a string with format ddMMyy into a DataTime.
        /// </summary>
        /// <param name="value">The string with the date in format ddMMyy.</param>
        /// <returns>The DateTime.</returns>
        public static DateTime StringToDate(
            string value)
        {
            if (value.Length != 6)
            {
                throw new Exception("Error converting to date, the string length must be 6.");
            }

            int day = Convert.ToInt32(value.Substring(0, 2));
            int month = Convert.ToInt32(value.Substring(2, 2));
            int year = 2000 + Convert.ToInt32(value.Substring(4, 2));

            return new DateTime(year, month, day);        
        }

        /// <summary>
        /// Converts a string with format yy into a integer.
        /// </summary>
        /// <param name="value">The string with the year in format yy.</param>
        /// <returns>The year as integer.</returns>
        public static int StringToYear(
            string value)
        {
            return Convert.ToInt32(value) + 2000;
        }

        /// <summary>
        /// Converts a string currency with format NNDD into a decimal.
        /// </summary>
        /// <param name="value">The string with format NNDD.</param>
        /// <returns>The value as decimal.</returns>
        public static decimal StringToCurrency(
            string value)
        {
            if (value.Length < 3)
            {
                throw new Exception("Error converting to currency, the min length is 3.");
            }

            decimal decimalPart = Convert.ToDecimal(value.Substring(value.Length - 2, 2)); 
            decimal integerPart = Convert.ToDecimal(value.Substring(0, value.Length - 2));

            return integerPart + decimalPart * 0.01m;
        }

        /// <summary>
        /// Gets CodigoTributo description.
        /// </summary>
        /// <param name="value">The CodigoTributo.</param>
        /// <returns>The string description.</returns>
        public static string CodigoTributoToString(
            string value)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            # region Conceptos Tributarios
            // TODO: Gets from a config file
            dictionary.Add("001", "Impuesto sobre bienes inmuebles (Urbana)");
            dictionary.Add("002", "Impuesto sobre bienes inmuebles (Rústica)");
            dictionary.Add("003", "Impuesto sobre vehículos de tracción mecánica");
            dictionary.Add("004", "Impuesto sobre incremento del valor de los terrenos");
            dictionary.Add("005", "Impuesto sobre construcciones, instalaciones y obras");
            dictionary.Add("010", "Impuesto sobre actividades económicas");
            dictionary.Add("011", "Tasa basuras domiciliarias");
            dictionary.Add("012", "Tasa de basuras comerciales");
            dictionary.Add("013", "Tasa basuras industriales");
            dictionary.Add("014", "Tasa eliminación residuos sólidos");
            dictionary.Add("015", "Tasa alcantarillado");
            dictionary.Add("016", "Tasa de cementerio municipal");
            dictionary.Add("017", "Tasa de inspección");
            dictionary.Add("018", "Tasa por expedición de documentos");
            dictionary.Add("019", "Tasa por licencia auto-taxi, vehículos alquiler");
            dictionary.Add("020", "Tasa por licencias urbanísticas");
            dictionary.Add("021", "Tasa por licencia apertura establecimientos");
            dictionary.Add("022", "Tasa por retirada vehículos vía pública");
            dictionary.Add("023", "Tasa de saneamiento");
            dictionary.Add("024", "Tasa por servicios espectáculos públicos");
            dictionary.Add("025", "Tasa de basuras de mercados municipales");
            dictionary.Add("041", "Precio Público por entrada vehículos (vados)");
            dictionary.Add("042", "Precio Público por suministro de aguas");
            dictionary.Add("043", "Precio Público por ocupación suelo, subsuelo y vuelo de la vía pública");
            dictionary.Add("044", "Precio Público por ocupación vía pública");
            dictionary.Add("045", "Precio Público por paradas mercado municipal");
            dictionary.Add("046", "Precio Público por guardería");
            dictionary.Add("047", "Precio Público por instalaciones deportivas");
            dictionary.Add("048", "Precio Público por matadero municipal");
            dictionary.Add("049", "Precio Público por prestaciones personales");
            dictionary.Add("050", "Precio Público por vigilancia");
            dictionary.Add("051", "Precio Público por comedores escolares");
            dictionary.Add("052", "Precio Público centros docentes escolares");
            dictionary.Add("053", "Precio Público sobre rodaje y arrastre de vehículos");
            dictionary.Add("054", "Precio Público lucha contra la piedra");
            dictionary.Add("055", "Precio Público por exhibición de rótulos");
            dictionary.Add("056", "Precio Público sobre rieles, cables, palos, etc.");
            dictionary.Add("057", "Precio Público por aprovechamientos especiales");
            dictionary.Add("058", "Precio Público instalación de aparadores y vitrinas");
            dictionary.Add("059", "Servicios sanitarios");
            dictionary.Add("060", "Precio Público sobre instalaciones en terrenos de uso público");
            dictionary.Add("061", "Contribuciones especiales");
            dictionary.Add("062", "I.V.A. Reducido");
            dictionary.Add("063", "I.V.A. Ordinario");
            dictionary.Add("064", "Multas");
            dictionary.Add("065", "Alquiler sepulturas");
            dictionary.Add("066", "Tenencia de perros");
            dictionary.Add("067", "Intereses de demora");
            dictionary.Add("068", "Recargo de apremio");
            dictionary.Add("069", "Máquinas recreativas");
            dictionary.Add("070", "Alta vehículos tracción mecánica");
            dictionary.Add("071", "Ingresos directos");
            dictionary.Add("072", "Bicicletas");
            dictionary.Add("073", "Otros ingresos tributarios");
            dictionary.Add("074", "Tasa garantía suministro");
            dictionary.Add("091", "Agrupación de tributos sobre industria-comercio");
            dictionary.Add("092", "Agrupación de tributos sobre inmuebles");
            dictionary.Add("096", "Agrupación conceptos en liquidaciones por ingreso directo");
            dictionary.Add("097", "Cámara propiedad urbana");
            dictionary.Add("098", "Cámara de comercio");
            dictionary.Add("099", "Cámara de comercio. Complemento");
            # endregion

            return dictionary[value];
        }
    }
}
