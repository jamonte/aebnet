﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

namespace AEBNET.Core
{
    /// <summary>
    /// Class CabeceraOrganismoEmisor.
    /// </summary>
    public class CabeceraOrganismoEmisor
    {
        public string OrganismoEmisor { get; set; }
        public string EntidadPresentadora { get; set; }
        public string OficinaPresentadora { get; set; }

        /// <summary>
        /// Gets the CabeceraOrganismoEmisor string.
        /// </summary>
        /// <returns>CabeceraOrganismoEmisor string.</returns>
        public override string ToString()
        {
            return
                " - Organismo Emisor: " + OrganismoEmisor + "\r\n" +
                " - Entidad Presentadora: " + EntidadPresentadora + "\r\n" +
                " - Oficina Presentadora: " + OficinaPresentadora;
        }
    }
}
