﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System;

namespace AEBNET.Core
{
    /// <summary>
    /// Class RegistroModalidad1.
    /// </summary>
    public class RegistroModalidad1 : RegistroBase
    {
        public string OrganismoEmisor { get; set; }
        public string Referencia { get; set; }
        public string EntidadRecaudadora { get; set; }
        public string OficinaRecaudadora { get; set; }
        public DateTime FechaCobro { get; set; }
        public decimal Importe { get; set; }
        public ModalidadesPago ModalidadPago { get; set; }
        public bool Domiciliacion { get; set; }
        public CodigoCuentaCliente CCCDomiciliacion { get; set; }        
        public string CodigoTributo { get; set; }
        public int Ejercicio { get; set; }
        public int Remesa { get; set; }

        /// <summary>
        /// Gets the RegistroModalidad1 string.
        /// </summary>
        /// <returns>RegistroModalidad1 string.</returns>
        public override string ToString()
        {
            return
                "[b]ORGANISMO EMISOR[/b]\r\n " + OrganismoEmisor + "\r\n" +
                "[b]REFERENCIA[/b]\r\n " + Referencia + "\r\n" +
                "[b]ENTIDAD RECAUDADORA[/b]\r\n " + EntidadRecaudadora + "\r\n" +
                "[b]OFICINA RECAUDADORA[/b]\r\n " + OficinaRecaudadora + "\r\n" +
                "[b]FECHA COBRO[/b]\r\n " + FechaCobro.ToShortDateString() + "\r\n" +
                "[b]IMPORTE[/b]\r\n " + Importe.ToString("C") + "\r\n" +
                "[b]MODALIDAD PAGO[/b]\r\n " + ModalidadPago.ToString() + "\r\n" +
                "[b]DOMICILIACION[/b]\r\n " + Domiciliacion + "\r\n" +
                "[b]CCC DOMICILIACION[/b]\r\n " + CCCDomiciliacion + "\r\n" +
                "[b]CODIGO TRIBUTO[/b]\r\n " + CodigoTributo + 
                " (" + Helper.CodigoTributoToString(CodigoTributo) + ")\r\n" +
                "[b]EJERCICIO[/b]\r\n " + Ejercicio + "\r\n" +
                "[b]REMESA[/b]\r\n " + Remesa;
        }

        /// <summary>
        /// Gets RegistroModalidad1 header string for the CSV file
        /// </summary>
        /// <returns>Header string</returns>
        public override string ToHeaderCSV()
        {
            return
                "Organismo Emisor;" +
                "Referencia;" +
                "Entidad Recaudadora;" +
                "Oficina Recaudadora;" +
                "Fecha Cobro;" +
                "Importe;" +
                "Modalidad Pago;" +
                "Domiciliacion;" +
                "CCC Domiciliacion;" +
                "Codigo Tributo;" +
                "Ejercicio;" +
                "Remesa;";
        }

        /// <summary>
        /// Gets RegistroModalidad1 record string for the CSV file
        /// </summary>
        /// <returns>Record string</returns>
        public override string ToRecordCSV()
        {
            return
                OrganismoEmisor + ";" +
                Referencia + ";" +
                EntidadRecaudadora + ";" +
                OficinaRecaudadora + ";" +
                FechaCobro.ToShortDateString() + ";" +
                Importe.ToString("N2") + ";" +
                ModalidadPago.ToString() + ";" +
                Domiciliacion + ";" +
                CCCDomiciliacion + ";" +
                CodigoTributo + ";" +
                Ejercicio + ";" +
                Remesa + ";";
        }
    }
}
