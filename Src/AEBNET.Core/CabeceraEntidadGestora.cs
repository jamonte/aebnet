﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System;

namespace AEBNET.Core
{
    /// <summary>
    /// Class CabeceraEntidadGestora.
    /// </summary>
    public class CabeceraEntidadGestora
    {
        public string EntidadGestora { get; set; }
        public string EntidadPresentadora { get; set; }
        public string OficinaPresentadora { get; set; }
        public DateTime FechaLiquidacion { get; set; }
        public CodigoCuentaCliente CodigoCuentaCliente { get; set; }

        /// <summary>
        /// Gets the CabeceraEntidadGestora string.
        /// </summary>
        /// <returns>CabeceraEntidadGestora string.</returns>
        public override string ToString()
        {
            return
                " - Entidad Gestora: " + EntidadGestora + "\r\n" +
                " - Entidad Presentadora: " + EntidadPresentadora + "\r\n" +
                " - Oficina Presentadora: " + OficinaPresentadora + "\r\n" +
                " - Fecha Liquidacion: " + FechaLiquidacion.ToShortDateString() + "\r\n" +
                " - Codigo Cuenta Cliente: " + CodigoCuentaCliente;
        }
    }
}
