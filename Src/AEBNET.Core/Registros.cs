﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System.Collections.Generic;

namespace AEBNET.Core
{
    /// <summary>
    /// Base class Registros.
    /// </summary>
    public class Registros
    {
        public List<RegistroBase> Conceptos { get; set; }
        public TotalTributo Total { get; set; }

        public Registros()
        {
            Conceptos = new List<RegistroBase>();
        }
    }
}
