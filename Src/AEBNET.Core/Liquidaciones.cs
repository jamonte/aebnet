﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace AEBNET.Core
{
    /// <summary>
    /// Class Liquidaciones.
    /// </summary>
    public class Liquidaciones
    {
        public Modalidades Modalidad { get; private set; }
        public CabeceraEntidadGestora Cabecera { get; set; }
        public List<OrganismoEmisor> Registros { get; set; }
        public TotalEntidadGestora Total { get; set; }

        public Liquidaciones(
            Modalidades modalidad)
        {
            Modalidad = modalidad;
            Registros = new List<OrganismoEmisor>();
        }

        /// <summary>
        /// Gets a collection with all concepts.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>ObservableCollection</returns>
        public ObservableCollection<T> ToCollection<T>()
        {
            ObservableCollection<T> list = new ObservableCollection<T>();

            foreach (OrganismoEmisor organismoEmisor in Registros)
            {
                foreach (Registros registro in organismoEmisor.Registros)
                {
                    foreach (object concepto in registro.Conceptos)
                    {
                        list.Add((T)concepto);
                    }
                }
            }
            
            return list;
        }

        /// <summary>
        /// Export the passengers collection to a CSV file.
        /// </summary>
        /// <param name="fileName">Output fileName.</param>
        public void ExportToCSV(
            string fileName)
        {
            using (StreamWriter file = new StreamWriter(fileName, false))
            {
                bool writeHeader = true;

                // Data
                foreach (OrganismoEmisor organismoEmisor in Registros)
                {
                    foreach (Registros registro in organismoEmisor.Registros)
                    {
                        foreach (RegistroBase concepto in registro.Conceptos)
                        {
                            // Header
                            if (writeHeader)
                            {
                                file.WriteLine(concepto.ToHeaderCSV());
                                writeHeader = false;
                            }

                            // Records
                            file.WriteLine(concepto.ToRecordCSV());
                        }
                    }
                }
            }
        }
    }
}
