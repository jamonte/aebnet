﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AEBNET.Core
{
    /// <summary>
    /// Class to process AEB-C60 files. Types M1, M2 and M3.
    /// </summary>
    public static class C60
    {
        #region Parse Liquidaciones
        private static void ParseEntidadGestora(
            Modalidades modalidad,
            int line,
            string text,
            Liquidaciones liquidaciones)
        {
            int offsetEntidadPresentadora = 28;
            int offsetOficinaPresentadora = 32;
            int offsetFechaLiquidacion = 36;
            int offsetCodigoCuentaCliente = 57;

            if (modalidad == Modalidades.M3)
            {
                offsetEntidadPresentadora = 23;
                offsetOficinaPresentadora = 27;
                offsetFechaLiquidacion = 31;
                offsetCodigoCuentaCliente = 51;
            }

            liquidaciones.Cabecera = new CabeceraEntidadGestora
            {
                EntidadGestora = text.Substring(4, 6),
                EntidadPresentadora = text.Substring(offsetEntidadPresentadora, 4),
                OficinaPresentadora = text.Substring(offsetOficinaPresentadora, 4),
                FechaLiquidacion = Helper.StringToDate(text.Substring(offsetFechaLiquidacion, 6)),
                CodigoCuentaCliente = new CodigoCuentaCliente(text.Substring(offsetCodigoCuentaCliente, 20))
            };
        }

        private static void ParseOrganismoEmisor(
            Modalidades modalidad,
            int line,
            string text,
            Liquidaciones liquidaciones)
        {
            int offsetEntidadPresentadora = 28;
            int offsetOficinaPresentadora = 32;

            if (modalidad == Modalidades.M3)
            {
                offsetEntidadPresentadora = 23;
                offsetOficinaPresentadora = 27;
            }

            OrganismoEmisor organismoEmisor = new OrganismoEmisor
            {
                Cabecera = new CabeceraOrganismoEmisor
                {
                    OrganismoEmisor = text.Substring(4, 6),
                    EntidadPresentadora = text.Substring(offsetEntidadPresentadora, 4),
                    OficinaPresentadora = text.Substring(offsetOficinaPresentadora, 4)
                }
            };

            liquidaciones.Registros.Add(organismoEmisor);
        }

        private static void ParseRegistroIndividualM1(
            int line,
            string text,
            Liquidaciones liquidaciones)
        {
            bool domiciliacion = false;
            CodigoCuentaCliente CCC = new CodigoCuentaCliente();

            // Codigo Domiciliacion
            if (text.Substring(56, 1) == "D")
            {
                domiciliacion = true;
                CCC = new CodigoCuentaCliente(text.Substring(57, 20));
            }

            RegistroModalidad1 registro = new RegistroModalidad1
            {
                OrganismoEmisor = text.Substring(4, 6),
                Referencia = text.Substring(13, 12),
                EntidadRecaudadora = text.Substring(28, 4),
                OficinaRecaudadora = text.Substring(32, 4),
                FechaCobro = Helper.StringToDate(text.Substring(36, 6)),
                Importe = Helper.StringToCurrency(text.Substring(42, 12)),
                ModalidadPago = (ModalidadesPago)Convert.ToInt32(text.Substring(55, 1)),
                Domiciliacion = domiciliacion,
                CCCDomiciliacion = CCC,
                CodigoTributo = text.Substring(77, 3),
                Ejercicio = Helper.StringToYear(text.Substring(80, 2)),
                Remesa = Convert.ToInt32(text.Substring(82, 2))
            };

            // First registro
            if (liquidaciones.Registros.Last().Registros.Count == 0)
            {
                liquidaciones.Registros.Last().Registros.Add(new Registros());
            }

            liquidaciones.Registros.Last().Registros.Last().Conceptos.Add(registro);
        }

        private static void ParseRegistroIndividualM2(
            int line,
            string text,
            Liquidaciones liquidaciones)
        {
            bool domiciliacion = false;
            CodigoCuentaCliente CCC = new CodigoCuentaCliente();

            // Codigo Domiciliacion
            if (text.Substring(56, 1) == "D")
            {
                domiciliacion = true;
                CCC = new CodigoCuentaCliente(text.Substring(57, 20));
            }

            RegistroModalidad2 registro = new RegistroModalidad2
            {
                OrganismoEmisor = text.Substring(4, 6),
                Referencia = text.Substring(13, 12),
                EntidadRecaudadora = text.Substring(28, 4),
                OficinaRecaudadora = text.Substring(32, 4),
                FechaCobro = Helper.StringToDate(text.Substring(36, 6)),
                Importe = Helper.StringToCurrency(text.Substring(42, 12)),
                ModalidadPago = (ModalidadesPago)Convert.ToInt32(text.Substring(55, 1)),
                Domiciliacion = domiciliacion,
                CCCDomiciliacion = CCC,
                CodigoTributo = text.Substring(77, 3),
                Ejercicio = Helper.StringToYear(text.Substring(80, 2)),
                AnoVencimiento = Convert.ToInt32(text.Substring(82, 1)),
                FechaJuliana = Convert.ToInt32(text.Substring(83, 3)),
                DiscriminantePeriodo = Convert.ToInt32(text.Substring(86, 1))
            };

            // First registro
            if (liquidaciones.Registros.Last().Registros.Count == 0)
            {
                liquidaciones.Registros.Last().Registros.Add(new Registros());
            }

            liquidaciones.Registros.Last().Registros.Last().Conceptos.Add(registro);
        }

        private static void ParseRegistroIndividualM3(
                    int line,
                    string text,
                    Liquidaciones liquidaciones)
        {
            DateTime? fechaDevengo = null;

            if (Convert.ToInt32(text.Substring(74, 6)) != 0)
            {
                fechaDevengo = Helper.StringToDate(text.Substring(74, 6));
            }

            RegistroModalidad3 registro = new RegistroModalidad3
            {
                OrganismoEmisor = text.Substring(4, 6),
                NumeroExpediente = text.Substring(11, 12),
                EntidadRecaudadora = text.Substring(23, 4),
                OficinaRecaudadora = text.Substring(27, 4),
                FechaCobro = Helper.StringToDate(text.Substring(31, 6)),
                Importe = Helper.StringToCurrency(text.Substring(37, 12)),
                NumeroJustificante = text.Substring(49, 13),
                NifCif = text.Substring(62, 9),
                CodigoModelo = text.Substring(71, 3),
                FechaDevengo = fechaDevengo,
                DatoEspecifico = text.Substring(80, 20)
            };

            // First registro
            if (liquidaciones.Registros.Last().Registros.Count == 0)
            {
                liquidaciones.Registros.Last().Registros.Add(new Registros());
            }

            liquidaciones.Registros.Last().Registros.Last().Conceptos.Add(registro);
        }

        private static void ParseRegistroIndividual(
            Modalidades modalidad,
            int line,
            string text,
            Liquidaciones liquidaciones)
        {
            switch (modalidad)
            {
                case Modalidades.M1:
                    ParseRegistroIndividualM1(line, text, liquidaciones);
                    break;

                case Modalidades.M2:
                    ParseRegistroIndividualM2(line, text, liquidaciones);
                    break;

                case Modalidades.M3:
                    ParseRegistroIndividualM3(line, text, liquidaciones);
                    break;
            }
        }

        private static void ParseTotalTributo(
            Modalidades modalidad,
            int line,
            string text,
            Liquidaciones liquidaciones)
        {
            int offsetNumeroCobros = 28;
            int offsetTotalImporte = 36;
            int offsetCodigoTributo = 77;

            if (modalidad == Modalidades.M3)
            {
                offsetNumeroCobros = 23;
                offsetTotalImporte = 31;
                offsetCodigoTributo = 71;
            }

            // First registro
            if (liquidaciones.Registros.Last().Registros.Count == 0)
            {
                liquidaciones.Registros.Last().Registros.Add(new Registros());
            }

            liquidaciones.Registros.Last().Registros.Last().Total = new TotalTributo
            {
                OrganismoEmisor = text.Substring(4, 6),
                NumeroCobros = Convert.ToInt32(text.Substring(offsetNumeroCobros, 8)),
                TotalImporte = Helper.StringToCurrency(text.Substring(offsetTotalImporte, 18)),
                CodigoTributo = text.Substring(offsetCodigoTributo, 3)
            };
        }

        private static void ParseTotalEntidadGestora(
            Modalidades modalidad,
            int line,
            string text,
            Liquidaciones liquidaciones)
        {
            int offsetNumeroRegistros = 28;
            int offsetTotalImporte = 36;

            if (modalidad == Modalidades.M3)
            {
                offsetNumeroRegistros = 23;
                offsetTotalImporte = 31;
            }

            liquidaciones.Total = new TotalEntidadGestora
            {
                OrganismoEmisor = text.Substring(4, 6),
                NumeroRegistros = Convert.ToInt32(text.Substring(offsetNumeroRegistros, 8)),
                TotalImporte = Helper.StringToCurrency(text.Substring(offsetTotalImporte, 18))
            };
        }

        private static void ParseLiquidaciones(
            Modalidades modalidad,
            int line,
            string text,
            Liquidaciones liquidaciones)
        {
            string codigo = text.Substring(0, 4);

            switch (codigo)
            {
                case "0170": // EntidadGestora
                case "0180":
                    ParseEntidadGestora(modalidad, line, text, liquidaciones);
                    break;

                case "0270": // OrganismoEmisor
                case "0280":
                    ParseOrganismoEmisor(modalidad, line, text, liquidaciones);
                    break;

                case "0370": // RegistroIndividual
                case "0380":
                    ParseRegistroIndividual(modalidad, line, text, liquidaciones);
                    break;

                case "0470": // TotalTributo
                case "0480":
                    ParseTotalTributo(modalidad, line, text, liquidaciones);
                    break;

                case "0570": // TotalEntidadGestora
                case "0580":
                    ParseTotalEntidadGestora(modalidad, line, text, liquidaciones);
                    break;

                default:
                    if (text.Replace(" ", string.Empty).Length > 0)
                    {
                        throw new Exception(String.Format("ERROR: Unexpected code at line {0:N0}", line));
                    }
                    break;
            }
        }
        # endregion

        /// <summary>
        /// Process a AEB-C60 file.
        /// </summary>
        /// <param name="modalidad">Modalidad type.</param>
        /// <param name="fileName">File name.</param>
        /// <returns>The Liquidaciones.</returns>
        public static Liquidaciones LoadLiquidaciones(
            Modalidades modalidad,
            string fileName)
        {
            Liquidaciones liquidaciones = new Liquidaciones(modalidad);
            int line = 1;

            try
            {
                using (var reader = new StreamReader(fileName))
                {
                    string text;

                    while ((text = reader.ReadLine()) != null)
                    {
                        ParseLiquidaciones(modalidad, line, text, liquidaciones);
                        line++;
                    }
                }

                return liquidaciones;
            }
            catch (Exception exception)
            {
                throw new Exception(String.Format(
                    "Error loading file {0} at line {1:N0}.\r\n{2}",
                    Path.GetFileName(fileName), line, exception.Message));
            }
        }

        /// <summary>
        /// Process async a AEB-C60 file.
        /// </summary>
        /// <param name="modalidad">Modalidad type.</param>
        /// <param name="fileName">File name.</param>
        /// <param name="token">Cancellation token.</param>
        /// <param name="progress">Line progress.</param>
        /// <returns>The Liquidaciones.</returns>
        public static async Task<Liquidaciones> LoadLiquidacionesAsync(
            Modalidades modalidad,
            string fileName,
            CancellationToken token,
            IProgress<int> progress)
        {
            Liquidaciones liquidaciones = new Liquidaciones(modalidad);
            int line = 1;

            try
            {
                await Task.Run(() =>
                {
                    using (var reader = new StreamReader(fileName))
                    {
                        string text;

                        while ((text = reader.ReadLine()) != null)
                        {
                            ParseLiquidaciones(modalidad, line, text, liquidaciones);

                            if (progress != null)
                            {
                                progress.Report(line);
                            }

                            if (token.IsCancellationRequested)
                            {
                                liquidaciones = null;
                                break;
                            }

                            line++;

                            // Uncomment the following line to show the progress dialog clearly
                            //Task.Delay(100).Wait();
                        }
                    }
                });
                
                return liquidaciones;
            }
            catch (Exception exception)
            {
                throw new Exception(String.Format(
                    "Error loading file {0} at line {1:N0}.\r\n{2}.",
                    Path.GetFileName(fileName), line, exception.Message));
            }
        }
    }
}
