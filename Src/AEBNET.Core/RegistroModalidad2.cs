﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System;

namespace AEBNET.Core
{
    /// <summary>
    /// Class RegistroModalidad2.
    /// </summary>
    public class RegistroModalidad2 : RegistroBase
    {
        public string OrganismoEmisor { get; set; }
        public string Referencia { get; set; }
        public string EntidadRecaudadora { get; set; }
        public string OficinaRecaudadora { get; set; }
        public DateTime FechaCobro { get; set; }
        public decimal Importe { get; set; }
        public ModalidadesPago ModalidadPago { get; set; }
        public bool Domiciliacion { get; set; }
        public CodigoCuentaCliente CCCDomiciliacion { get; set; }
        public string CodigoTributo { get; set; }
        public int Ejercicio { get; set; }
        public int AnoVencimiento { get; set; }
        public int FechaJuliana { get; set; }
        public int DiscriminantePeriodo { get; set; }

        /// <summary>
        /// Gets the RegistroModalidad2 string.
        /// </summary>
        /// <returns>RegistroModalidad2 string.</returns>
        public override string ToString()
        {
            return
                "[b]ORGANISMO EMISOR[/b]\r\n  " + OrganismoEmisor + "\r\n" +
                "[b]REFERENCIA[/b]\r\n  " + Referencia + "\r\n" +
                "[b]ENTIDAD RECAUDADORA[/b]\r\n  " + EntidadRecaudadora + "\r\n" +
                "[b]OFICINA RECAUDADORA[/b]\r\n  " + OficinaRecaudadora + "\r\n" +
                "[b]FECHA COBRO[/b]\r\n  " + FechaCobro.ToShortDateString() + "\r\n" +
                "[b]IMPORTE[/b]\r\n  " + Importe.ToString("C") + "\r\n" +
                "[b]MODALIDAD PAGO[/b]\r\n  " + ModalidadPago.ToString() + "\r\n" +
                "[b]DOMICILIACION[/b]\r\n  " + Domiciliacion + "\r\n" +
                "[b]CCC DOMICILIACION[/b]\r\n  " + CCCDomiciliacion + "\r\n" +
                "[b]CODIGO TRIBUTO[/b]\r\n  " + CodigoTributo + 
                " (" + Helper.CodigoTributoToString(CodigoTributo) + ")\r\n" +
                "[b]EJERCICIO[/b]\r\n  " + Ejercicio + "\r\n" +
                "[b]ANO VENCIMIENTO[/b]\r\n  " + AnoVencimiento + "\r\n" +
                "[b]FECHA JULIANA[/b]\r\n  " + FechaJuliana + "\r\n" +
                "[b]DISCRIMINANTE PERIODO[/b]\r\n  " + DiscriminantePeriodo;
        }

        /// <summary>
        /// Gets RegistroModalidad2 header string for the CSV file
        /// </summary>
        /// <returns>Header string</returns>
        public override string ToHeaderCSV()
        {
            return
                "Organismo Emisor;" +
                "Referencia;" +
                "Entidad Recaudadora;" +
                "Oficina Recaudadora;" +
                "Fecha Cobro;" +
                "Importe;" +
                "Modalidad Pago;" +
                "Domiciliacion;" +
                "CCC Domiciliacion;" +
                "Codigo Tributo;" +
                "Ejercicio;" +
                "Ano Vencimiento;" +
                "Fecha Juliana;" +
                "Discriminante Periodo;";            
        }

        /// <summary>
        /// Gets RegistroModalidad2 record string for the CSV file
        /// </summary>
        /// <returns>Record string</returns>
        public override string ToRecordCSV()
        {
            return
                OrganismoEmisor + ";" +
                Referencia + ";" +
                EntidadRecaudadora + ";" +
                OficinaRecaudadora + ";" +
                FechaCobro.ToShortDateString() + ";" +
                Importe.ToString("N2") + ";" +
                ModalidadPago.ToString() + ";" +
                Domiciliacion + ";" +
                CCCDomiciliacion + ";" +
                CodigoTributo + ";" +
                Ejercicio + ";" +
                AnoVencimiento + ";" +
                FechaJuliana + ";" +
                DiscriminantePeriodo + ";";
        }
    }
}
