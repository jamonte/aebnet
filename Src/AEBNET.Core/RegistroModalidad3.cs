﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using System;

namespace AEBNET.Core
{
    /// <summary>
    /// Class RegistroModalidad3.
    /// </summary>
    public class RegistroModalidad3 : RegistroBase
    {
        public string OrganismoEmisor { get; set; }
        public string NumeroExpediente { get; set; }
        public string EntidadRecaudadora { get; set; }
        public string OficinaRecaudadora { get; set; }
        public DateTime FechaCobro { get; set; }
        public decimal Importe { get; set; }
        public string NumeroJustificante { get; set; }
        public string NifCif { get; set; }
        public string CodigoModelo { get; set; }
        public DateTime? FechaDevengo { get; set; }
        public string DatoEspecifico { get; set; }

        /// <summary>
        /// Gets the RegistroModalidad3 string.
        /// </summary>
        /// <returns>RegistroModalidad3 string.</returns>
        public override string ToString()
        {
            string strFechaDevengo = "N/A";

            if (FechaDevengo.HasValue)
            {
                strFechaDevengo = FechaDevengo.Value.ToShortDateString();
            }

            return
                "[b]ORGANISMO EMISOR[/b]\r\n " + OrganismoEmisor + "\r\n" +
                "[b]NUMERO EXPEDIENTE[/b]\r\n " + NumeroExpediente + "\r\n" +
                "[b]ENTIDAD RECAUDADORA[/b]\r\n " + EntidadRecaudadora + "\r\n" +
                "[b]OFICINA RECAUDADORA[/b]\r\n " + OficinaRecaudadora + "\r\n" +
                "[b]FECHA COBRO[/b]\r\n " + FechaCobro.ToShortDateString() + "\r\n" +
                "[b]IMPORTE[/b]\r\n " + Importe.ToString("C") + "\r\n" +
                "[b]NUMERO JUSTIFICANTE[/b]\r\n " + NumeroJustificante + "\r\n" +
                "[b]NIF/CIF[/b]\r\n " + NifCif + "\r\n" +
                "[b]CODIGO MODELO[/b]\r\n " + CodigoModelo + "\r\n" +
                "[b]FECHA DEVENGO[/b]\r\n " + strFechaDevengo + "\r\n" +
                "[b]DATO ESPECIFICO[/b]\r\n " + DatoEspecifico;
        }

        /// <summary>
        /// Gets RegistroModalidad3 header string for the CSV file
        /// </summary>
        /// <returns>Header string</returns>
        public override string ToHeaderCSV()
        {
            return
                "Organismo Emisor;" +
                "Numero Expediente;" +
                "Entidad Recaudadora;" +
                "Oficina Recaudadora;" +
                "Fecha Cobro;" +
                "Importe;" +
                "Numero Justificante;" +
                "Nif/Cif;" +
                "Codigo Modelo;" +
                "Fecha Devengo;" +
                "Dato Especifico;";
        }

        /// <summary>
        /// Gets RegistroModalidad3 record string for the CSV file
        /// </summary>
        /// <returns>Record string</returns>
        public override string ToRecordCSV()
        {
            string strFechaDevengo = "N/A";

            if (FechaDevengo.HasValue)
            {
                strFechaDevengo = FechaDevengo.Value.ToShortDateString();
            }

            return
                OrganismoEmisor + ";" +
                NumeroExpediente + ";" +
                EntidadRecaudadora + ";" +
                OficinaRecaudadora + ";" +
                FechaCobro.ToShortDateString() + ";" +
                Importe.ToString("N2") + ";" +
                NumeroJustificante + ";" +
                NifCif + ";" +
                CodigoModelo + ";" +
                strFechaDevengo + ";" +
                DatoEspecifico + ";";
        }
    }
}
