﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

namespace AEBNET.Core
{
    /// <summary>
    /// Class TotalEntidadGestora.
    /// </summary>
    public class TotalEntidadGestora
    {
        public string OrganismoEmisor { get; set; }
        public int NumeroRegistros { get; set; }
        public decimal TotalImporte { get; set; }

        /// <summary>
        /// Gets the TotalEntidadGestora string.
        /// </summary>
        /// <returns>TotalEntidadGestora string.</returns>
        public override string ToString()
        {
            return
                " - Organismo Emisor: " + OrganismoEmisor + "\r\n" +
                " - Numero Registros: " + NumeroRegistros + "\r\n" +
                " - Total Importe: " + TotalImporte.ToString("C");
        }
    }   
}
