﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AEBNET.Core.Test
{
    [TestClass]
    public class UnitTestCodigoCuentaCliente
    {
        [TestMethod]
        public void TestCodigoCuentaCliente()
        {
            CodigoCuentaCliente codigoCuentaCliente = new CodigoCuentaCliente("1234-5678-06-1234567890");

            Assert.AreEqual(codigoCuentaCliente.Entidad, "1234");
            Assert.AreEqual(codigoCuentaCliente.Oficina, "5678");
            Assert.AreEqual(codigoCuentaCliente.DigitoControl, "06");
            Assert.AreEqual(codigoCuentaCliente.NumeroCuenta, "1234567890");
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void TestCodigoCuentaClienteExceptionDC()
        {
            new CodigoCuentaCliente("1234-5678-06-1234567891");
            Assert.Fail("Exception was expected.");            
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestCodigoCuentaClienteExceptionLength()
        {
            new CodigoCuentaCliente("1234-5678-06-12345678901");
            Assert.Fail("Exception was expected.");
        }
    }
}
