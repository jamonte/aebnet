﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AEBNET.Core.Test
{
    [TestClass]
    public class UnitTestHelper
    {
        [TestMethod]
        public void TestStringToDate()
        {
            Assert.AreEqual(new DateTime(2014, 3, 29), Helper.StringToDate("290314"));
        }

        [TestMethod]
        public void TestStringToYear()
        {
            Assert.AreEqual(2014, Helper.StringToYear("14"));
        }

        [TestMethod]
        public void TestStringToCurrency()
        {
            Assert.AreEqual(105.98m, Helper.StringToCurrency("000010598"));
        }
    }
}
