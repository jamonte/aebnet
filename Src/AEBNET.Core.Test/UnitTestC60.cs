﻿// ==============================================================================
//  AEBNET
//
//  José Ángel Montelongo Reyes - email[AT]jamontelongo[DOT]info
//
//  Dec 2.014	  	  
// ==============================================================================

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AEBNET.Core.Test
{
    [TestClass]
    public class UnitTestC60
    {
        private const string fileM1 = @"..\..\..\..\Samples\Test\M1-C60m1_0049_20091120_0.val";
        private const string fileM1Error = @"..\..\..\..\Samples\Test\M1-Error.C60m1_0049_20091120_0.val";

        private const string fileM2 = @"..\..\..\..\Samples\Test\M2-C60m2_0061_20091120_0.val";
        private const string fileM2Error = @"..\..\..\..\Samples\Test\M2-Error.C60m2_0061_20091120_0.val";

        private const string fileM3 = @"..\..\..\..\Samples\Test\M3-C60m3_0182_20091120_0.val";
        private const string fileM3Error = @"..\..\..\..\Samples\Test\M3-Error.C60m3_0182_20091120_0.val";

        #region Test: M1
        private void CheckLiquidacionesM1(
            Liquidaciones liquidaciones)
        {
            Assert.IsTrue(
                ((liquidaciones.Cabecera.EntidadGestora == "353371") &&
                 (liquidaciones.Cabecera.EntidadPresentadora == "0049") &&
                 (liquidaciones.Cabecera.OficinaPresentadora == "1881") &&
                 (liquidaciones.Cabecera.FechaLiquidacion == new DateTime(2009, 11, 19)) &&
                 (liquidaciones.Cabecera.CodigoCuentaCliente.ToString() == "0000 0000 00 0000000000")));

            Assert.IsTrue(
                ((liquidaciones.Total.OrganismoEmisor == "353371") &&
                 (liquidaciones.Total.NumeroRegistros == 66) &&
                 (liquidaciones.Total.TotalImporte == 963.57m)));

            Assert.IsTrue(
                ((liquidaciones.Registros[0].Cabecera.OrganismoEmisor == "353371") &&
                 (liquidaciones.Registros[0].Cabecera.EntidadPresentadora == "0049") &&
                 (liquidaciones.Registros[0].Cabecera.OficinaPresentadora == "1881")));

            Assert.IsTrue(
                ((liquidaciones.Registros[0].Registros[0].Total.OrganismoEmisor == "353371") &&
                 (liquidaciones.Registros[0].Registros[0].Total.NumeroCobros == 62) &&
                 (liquidaciones.Registros[0].Registros[0].Total.TotalImporte == 963.57m) &&
                 (liquidaciones.Registros[0].Registros[0].Total.CodigoTributo == "073")));

            Assert.IsTrue(liquidaciones.Registros[0].Registros[0].Conceptos.Count == 62);

            RegistroModalidad1 registroM1 = liquidaciones.Registros[0].Registros[0].Conceptos[61] as RegistroModalidad1;

            Assert.IsTrue(
                ((registroM1.OrganismoEmisor == "353371") &&
                 (registroM1.Referencia == "100142877432") &&
                 (registroM1.EntidadRecaudadora == "0049") &&
                 (registroM1.OficinaRecaudadora == "0323") &&
                 (registroM1.FechaCobro == new DateTime(2009, 11, 19)) &&
                 (registroM1.Importe == 16m) &&
                 (registroM1.ModalidadPago == ModalidadesPago.Ventanilla) &&
                 (registroM1.Domiciliacion == false) &&
                 (registroM1.CCCDomiciliacion.ToString() == "0000 0000 00 0000000000") &&
                 (registroM1.CodigoTributo == "073") &&
                 (registroM1.Ejercicio == 2009) &&
                 (registroM1.Remesa == 13)));
        }

        [TestMethod]
        public void TestLoadModalidad1()
        {
            CheckLiquidacionesM1(C60.LoadLiquidaciones(Modalidades.M1, fileM1));
        }

        [TestMethod]
        public async Task TestLoadModalidad1Async()
        {
            var cts = new CancellationTokenSource();
            var progressIndicator = new Progress<int>();

            CheckLiquidacionesM1(await C60.LoadLiquidacionesAsync(Modalidades.M1, fileM1, cts.Token, progressIndicator));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestLoadModalidad1Exception()
        {
            CheckLiquidacionesM1(C60.LoadLiquidaciones(Modalidades.M1, fileM1Error));
        }
        #endregion

        #region Test: M2
        private void CheckLiquidacionesM2(
            Liquidaciones liquidaciones)
        {
            Assert.IsTrue(
                ((liquidaciones.Cabecera.EntidadGestora == "353383") &&
                 (liquidaciones.Cabecera.EntidadPresentadora == "0061") &&
                 (liquidaciones.Cabecera.OficinaPresentadora == "0515") &&
                 (liquidaciones.Cabecera.FechaLiquidacion == new DateTime(2009, 11, 19)) &&
                 (liquidaciones.Cabecera.CodigoCuentaCliente.ToString() == "0000 0000 00 0000000000")));

            Assert.IsTrue(
                ((liquidaciones.Total.OrganismoEmisor == "353383") &&
                 (liquidaciones.Total.NumeroRegistros == 11) &&
                 (liquidaciones.Total.TotalImporte == 602.37m)));

            Assert.IsTrue(
                ((liquidaciones.Registros[0].Cabecera.OrganismoEmisor == "353383") &&
                 (liquidaciones.Registros[0].Cabecera.EntidadPresentadora == "0061") &&
                 (liquidaciones.Registros[0].Cabecera.OficinaPresentadora == "0515")));

            Assert.IsTrue(
                ((liquidaciones.Registros[0].Registros[0].Total.OrganismoEmisor == "353383") &&
                 (liquidaciones.Registros[0].Registros[0].Total.NumeroCobros == 7) &&
                 (liquidaciones.Registros[0].Registros[0].Total.TotalImporte == 602.37m) &&
                 (liquidaciones.Registros[0].Registros[0].Total.CodigoTributo == "073")));

            Assert.IsTrue(liquidaciones.Registros[0].Registros[0].Conceptos.Count == 7);

            RegistroModalidad2 registroM2 = liquidaciones.Registros[0].Registros[0].Conceptos[6] as RegistroModalidad2;

            Assert.IsTrue(
                ((registroM2.OrganismoEmisor == "353383") &&
                 (registroM2.Referencia == "000035115176") &&
                 (registroM2.EntidadRecaudadora == "0061") &&
                 (registroM2.OficinaRecaudadora == "0273") &&
                 (registroM2.FechaCobro == new DateTime(2009, 11, 19)) &&
                 (registroM2.Importe == 82.08m) &&
                 (registroM2.ModalidadPago == ModalidadesPago.Ventanilla) &&
                 (registroM2.Domiciliacion == false) &&
                 (registroM2.CCCDomiciliacion.ToString() == "0000 0000 00 0000000000") &&
                 (registroM2.CodigoTributo == "073") &&
                 (registroM2.Ejercicio == 2009) &&
                 (registroM2.AnoVencimiento == 9) &&
                 (registroM2.FechaJuliana == 343) &&
                 (registroM2.DiscriminantePeriodo == 1)));
        }

        [TestMethod]
        public void TestLoadModalidad2()
        {
            CheckLiquidacionesM2(C60.LoadLiquidaciones(Modalidades.M2, fileM2));
        }

        [TestMethod]
        public async Task TestLoadModalidad2Async()
        {
            var cts = new CancellationTokenSource();
            var progressIndicator = new Progress<int>();

            CheckLiquidacionesM2(await C60.LoadLiquidacionesAsync(Modalidades.M2, fileM2, cts.Token, progressIndicator));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestLoadModalidad2Exception()
        {
            CheckLiquidacionesM2(C60.LoadLiquidaciones(Modalidades.M2, fileM2Error));
        }
        #endregion

        #region Test: M3
        private void CheckLiquidacionesM3(
                    Liquidaciones liquidaciones)
        {
            Assert.IsTrue(
                ((liquidaciones.Cabecera.EntidadGestora == "353395") &&
                 (liquidaciones.Cabecera.EntidadPresentadora == "0182") &&
                 (liquidaciones.Cabecera.OficinaPresentadora == "0062") &&
                 (liquidaciones.Cabecera.FechaLiquidacion == new DateTime(2009, 11, 19)) &&
                 (liquidaciones.Cabecera.CodigoCuentaCliente.ToString() == "0000 0000 00 0000000000")));

            Assert.IsTrue(
                ((liquidaciones.Total.OrganismoEmisor == "353395") &&
                 (liquidaciones.Total.NumeroRegistros == 8) &&
                 (liquidaciones.Total.TotalImporte == 120m)));

            Assert.IsTrue(
                ((liquidaciones.Registros[0].Cabecera.OrganismoEmisor == "353395") &&
                 (liquidaciones.Registros[0].Cabecera.EntidadPresentadora == "0182") &&
                 (liquidaciones.Registros[0].Cabecera.OficinaPresentadora == "0062")));

            Assert.IsTrue(
                ((liquidaciones.Registros[0].Registros[0].Total.OrganismoEmisor == "353395") &&
                 (liquidaciones.Registros[0].Registros[0].Total.NumeroCobros == 4) &&
                 (liquidaciones.Registros[0].Registros[0].Total.TotalImporte == 120m) &&
                 (liquidaciones.Registros[0].Registros[0].Total.CodigoTributo == "421")));

            Assert.IsTrue(liquidaciones.Registros[0].Registros[0].Conceptos.Count == 4);

            RegistroModalidad3 registroM3 = liquidaciones.Registros[0].Registros[0].Conceptos[3] as RegistroModalidad3;

            Assert.IsTrue(
                ((registroM3.OrganismoEmisor == "353395") &&
                 (registroM3.NumeroExpediente == "            ") &&
                 (registroM3.EntidadRecaudadora == "0182") &&
                 (registroM3.OficinaRecaudadora == "6876") &&
                 (registroM3.FechaCobro == new DateTime(2009, 11, 19)) &&
                 (registroM3.Importe == 30m) &&
                 (registroM3.NumeroJustificante == "4214000006005") &&
                 (registroM3.NifCif == "B35788165") &&
                 (registroM3.CodigoModelo == "421") &&
                 (registroM3.FechaDevengo == new DateTime(2001, 1, 1)) &&
                 (registroM3.DatoEspecifico == "4214000006005       ")));
        }

        [TestMethod]
        public void TestLoadModalidad3()
        {
            CheckLiquidacionesM3(C60.LoadLiquidaciones(Modalidades.M3, fileM3));
        }

        [TestMethod]
        public async Task TestLoadModalidad3Async()
        {
            var cts = new CancellationTokenSource();
            var progressIndicator = new Progress<int>();

            CheckLiquidacionesM3(await C60.LoadLiquidacionesAsync(Modalidades.M3, fileM3, cts.Token, progressIndicator));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestLoadModalidad3Exception()
        {
            CheckLiquidacionesM3(C60.LoadLiquidaciones(Modalidades.M3, fileM3Error));
        }
        #endregion
    }
}
