﻿  ----------
    AEBNET
  ----------

   - Version: 0.1.0 
   - Last Updated: 29/01/2015
   - Author: José Ángel Montelongo Reyes
   - Email: email[AT]jamontelongo[DOT]info
   - Web: http://jamontelongo.info

   AEBNET is a library to process C60 files that uses the AEB (Spanish Banking Association) standard
   (https://empresa.lacaixa.es/deployedfiles/empresas/Estaticos/pdf/Transferenciasyficheros/C-60.pdf)   
   The library can handle all types (M1, M2 and M3) and also allows export the data to CSV files.

   Example:
	liquidaciones = C60.LoadLiquidaciones(Modalidades.M1, "filename.M1");
	liquidaciones.ExportToCSV("filename.csv");			

   Async example:	
	var cts = new CancellationTokenSource();
	var progressIndicator = new Progress<int>();
	
	var liquidaciones = await C60.LoadLiquidacionesAsync(Modalidades.M1, "filename.M1", token, progress);
	liquidaciones.ExportToCSV("filename.csv");

	AEBNET use:

	- MVVM Light Toolkit (http://www.mvvmlight.net)
	- Modern UI for WPF (https://mui.codeplex.com)
	- Modern UI Icons (http://modernuiicons.com)
